## 图片裁切

提供基础的图片修改功能

### 单独使用方法

:::demo

```html
<template>
  <els-image-cut @success="handleSuccess" :imgWidth="300" :imgHeight="300" outputType="png">
    <el-button v-if="!headImg" type="primary">选择图片</el-button>
    <img v-else :src="headImg" alt="" />
  </els-image-cut>
</template>

<script>
  export default {
    data() {
      return {
        headImg: null
      }
    },
    methods: {
      handleSuccess(e) {
        this.headImg = e.url
      }
    }
  }
</script>
```

:::

### 与 el-upload / els-upload 配合实现图片裁切上传（此处示例可上传多图）

####

:::demo 在 upload 中内嵌一个 image-cut 组件，把 upload 的 trigger 插槽置空，用于取消 upload 组件的点击选择文件。在 image-cut 的裁切成功回调中将 file 作为参数，调用 upload 中的 uploadFiles 方法，手动触发文件上传。特别注意：uploadFiles 接收的参数是个数组

```html
<template>
  <div>
    <el-upload v-show="false" ref="upload" :show-file-list="false" :on-success="onSuccess" action="http://192.168.28.24:20001/base-server/file/uploadSingle" />
    <els-image-cut :img-width="200" :img-height="200" output-type="png" @success="handleSuccess">
      <el-button slot="trigger" type="primary">选择图片</el-button>
      <img v-for="item in headImg" :key="item" :src="item" alt="" style="margin: 10px 10px 0 0" />
    </els-image-cut>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        headImg: []
      }
    },
    methods: {
      handleSuccess(e) {
        this.$refs.upload.$refs['upload-inner'].uploadFiles([e.file])
        this.headImg.push(e.url)
      },
      onSuccess(e) {
        console.log(e)
        this.$message.success('上传成功')
      }
    }
  }
</script>
```

:::

### Attribute

<!-- prettier-ignore -->
| 参数      | 说明          | 类型      | 可选值                           | 默认值  |
|---------- |-------------- |---------- |--------------------------------  |-------- |
| imgWidth | 裁切之后返回的图片宽度 | Number | — | 200 |
| imgHeight | 裁切之后返回的图片高度 | Number | — | 200 |
| outputType | 裁切之后返回的图片类型 | String | jpg,png | jpg |

### Events

<!-- prettier-ignore -->
| 事件名      | 说明          | 参数       |
|---------- |-------------- |----------  |
| success | 图片裁切完成之后触发的时间 | { url, blob, file, base64 }  |

### Slot

<!-- prettier-ignore -->
| name | 说明 |
|------|--------|
| trigger | 触发文件选择框的内容 |
