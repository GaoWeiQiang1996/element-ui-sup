## 快速上手

本节将介绍如何在项目中使用 element-ui-sup

### 安装 element-ui-sup

```bash
npm i element-ui-sup -S
```

### 引入 element-ui-sup

在 main.js 中写入以下内容：

```js
import Vue from 'vue'
import ElementUiSup from 'element-ui-sup'
import 'element-ui-sup/lib/style/index.css' //引入样式文件
import App from './App.vue'

Vue.use(ElementUiSup)

new Vue({
  el: '#app',
  render: (h) => h(App)
})
```

element-ui-sup 的使用环境就初始化完毕了，现在就可以编写代码了，各个组件的使用方法请查阅它们各自的文档
