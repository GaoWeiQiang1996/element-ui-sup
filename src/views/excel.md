## excel 表格的导入与导出

提供与 excel 表格相关的操作组件

### 基本导出

:::demo

```html
<template>
  <div>
    <els-excel :data="data" :header="header" :on-export-success="exportSuccess">
      <el-button type="primary" slot="export">导出</el-button>
    </els-excel>
    <el-table style="margin-top:10px;" :data="data">
      <el-table-column v-for="item in header" :key="item.prop" :prop="item.prop" :label="item.label" />
    </el-table>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        data: [
          { id: 1, date: '2020/11/19', name: '小高', height: '111CM', weight: '52KG' },
          { id: 2, date: '2020/11/20', name: '小小高', height: '222CM', weight: '55KG' },
          { id: 3, date: '2020/11/11', name: '小林', height: '333CM', weight: '60KG' },
          { id: 4, date: '2020/11/21', name: '小小林', height: '444KCM', weight: '55KG' },
          { id: 5, date: '2020/11/23', name: '小王', height: '555CM', weight: '12KG' },
          { id: 6, date: '2020/11/24', name: '小小王', height: '556CM', weight: '22KG' },
          { id: 7, date: '2020/11/26', name: '大大王', height: '666CM', weight: '33KG' }
        ],
        header: [
          { prop: 'id', label: '序号' },
          { prop: 'name', label: '姓名' },
          { prop: 'date', label: '日期' },
          { prop: 'height', label: '身高' },
          { prop: 'weight', label: '体重' }
        ]
      }
    },
    methods: {
      exportSuccess(file) {
        console.log('导出成功', file)
      }
    }
  }
</script>
```

:::

### 基本导入

:::demo 导入默认获取第一张工作表数据，只支持单表头，如果选择多表头的文件,第一行之外的表头会被当成数据，

```html
<template>
  <div>
    <els-excel :data="data" :header="header" :on-import-success="importSuccess">
      <el-button type="primary" slot="import">导入</el-button>
    </els-excel>
    <el-table style="margin-top:10px;" :data="data">
      <el-table-column v-for="item in header" :key="item.prop" :prop="item.prop" :label="item.label" />
    </el-table>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        data: [],
        header: []
      }
    },
    methods: {
      importSuccess(data, header) {
        console.log('导入成功', data, header)
        this.data = data
        this.header = header
      }
    }
  }
</script>
```

:::

### 自定义导出的表头

:::demo 传入 multiHeader(多表头)与 merges(合并的单元格)来导出特殊的表头

```html
<template>
  <div>
    <els-excel :data="data" :header="header" :on-export-success="exportSuccess" :multi-header="multiHeader" :merges="merges">
      <el-button type="primary" slot="export">导出特殊表头</el-button>
    </els-excel>
    <el-table style="margin-top:10px;" :data="data">
      <el-table-column label="体检信息">
        <el-table-column prop="id" label="序号" />
        <el-table-column prop="name" label="姓名" />
        <el-table-column prop="date" label="日期" />
        <el-table-column label="个人信息">
          <el-table-column prop="height" label="身高" />
          <el-table-column prop="weight" label="体重" />
        </el-table-column>
      </el-table-column>
    </el-table>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        data: [
          { id: 1, date: '2020/11/19', name: '小高', height: '111CM', weight: '52KG' },
          { id: 2, date: '2020/11/20', name: '小小高', height: '222CM', weight: '55KG' },
          { id: 3, date: '2020/11/11', name: '小林', height: '333CM', weight: '60KG' },
          { id: 4, date: '2020/11/21', name: '小小林', height: '444KCM', weight: '55KG' },
          { id: 5, date: '2020/11/23', name: '小王', height: '555CM', weight: '12KG' },
          { id: 6, date: '2020/11/24', name: '小小王', height: '556CM', weight: '22KG' },
          { id: 7, date: '2020/11/26', name: '大大王', height: '666CM', weight: '33KG' }
        ],
        header: [
          { prop: 'id', label: '序号' },
          { prop: 'name', label: '姓名' },
          { prop: 'date', label: '日期' },
          { prop: 'height', label: '身高' },
          { prop: 'weight', label: '体重' }
        ],
        multiHeader: [['体检信息'], ['序号', '姓名', '日期', '个人信息'], ['', '', '', '身高', '体重']],
        merges: ['A1:E1', 'A2:A3', 'B2:B3', 'C2:C3', 'D2:E2']
      }
    },
    methods: {
      exportSuccess(file) {
        console.log('导出成功', file)
      }
    }
  }
</script>
```

:::

### 异步获取导出数据

:::demo data 传入一个异步方法，此方法异步返回一个数组即可

```html
<template>
  <div>
    <els-excel :data="getData" :header="header" :on-export-success="exportSuccess">
      <el-button type="primary" slot="export">导出</el-button>
    </els-excel>
    <el-table :data="data">
      <el-table-column v-for="item in header" :key="item.prop" :prop="item.prop" :label="item.label" />
    </el-table>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        data: [],
        header: [
          { prop: 'id', label: '序号' },
          { prop: 'name', label: '姓名' },
          { prop: 'date', label: '日期' },
          { prop: 'height', label: '身高' },
          { prop: 'weight', label: '体重' }
        ]
      }
    },
    async created() {
      this.data = await this.getData()
    },
    methods: {
      exportSuccess(file) {
        console.log('导出成功', file)
      },
      getData() {
        return new Promise((resolve) => {
          //模拟异步请求
          setTimeout(() => {
            resolve([
              { id: 1, date: '2020/11/19', name: '小高', height: '111CM', weight: '52KG' },
              { id: 2, date: '2020/11/20', name: '小小高', height: '222CM', weight: '55KG' },
              { id: 3, date: '2020/11/11', name: '小林', height: '333CM', weight: '60KG' },
              { id: 4, date: '2020/11/21', name: '小小林', height: '444KCM', weight: '55KG' },
              { id: 5, date: '2020/11/23', name: '小王', height: '555CM', weight: '12KG' },
              { id: 6, date: '2020/11/24', name: '小小王', height: '556CM', weight: '22KG' },
              { id: 7, date: '2020/11/26', name: '大大王', height: '666CM', weight: '33KG' }
            ])
          }, 1500)
        })
      }
    }
  }
</script>
```

:::

### 导出多个工作表

:::tip
导出多工作表时，将原先需要传 Array 的项，改传 Object，Object 每一项对应一个原先的 Array
:::
:::demo

```html
<template>
  <div>
    <els-excel :data="{ studentInfo, classInfo }" :header="header" :on-export-success="exportSuccess" :sheet-name="sheetName" :on-export-error="erportError" :multi-header="multiHeader" :merges="merges">
      <el-button slot="export">同时导出多工作表</el-button>
    </els-excel>
    <el-card style="margin-top: 10px">
      <div style="margin-bottom: 15px">表格1：</div>
      <el-table :data="studentInfo">
        <el-table-column label="学生信息">
          <el-table-column v-for="item in header.studentInfo" :key="item.id" v-bind="item" />
        </el-table-column>
      </el-table>
    </el-card>
    <el-card style="margin-top: 10px">
      <div style="margin-bottom: 15px">表格2：</div>
      <el-table :data="classInfo">
        <el-table-column label="班级信息">
          <el-table-column v-for="item in header.classInfo" :key="item.id" v-bind="item" />
        </el-table-column>
      </el-table>
    </el-card>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        studentInfo: [
          { id: 10001, date: '2020/11/19', name: '小高', height: '111CM', weight: '52KG' },
          { id: 10002, date: '2020/11/20', name: '小小高', height: '222CM', weight: '55KG' },
          { id: 10003, date: '2020/11/11', name: '小林', height: '333CM', weight: '60KG' },
          { id: 10004, date: '2020/11/21', name: '小小林', height: '444KCM', weight: '55KG' },
          { id: 10005, date: '2020/11/23', name: '小王', height: '555CM', weight: '12KG' },
          { id: 10006, date: '2020/11/24', name: '小小王', height: '556CM', weight: '22KG' },
          { id: 10007, date: '2020/11/26', name: '大大王', height: '666CM', weight: '33KG' }
        ],
        classInfo: [
          { id: 1, grade: '二年级', class: '六班', num: '55人' },
          { id: 2, grade: '三年级', class: '六班', num: '52人' },
          { id: 3, grade: '五年级', class: '六班', num: '54人' },
          { id: 4, grade: '七年级', class: '六班', num: '60人' },
          { id: 5, grade: '八年级', class: '六班', num: '50人' },
          { id: 6, grade: '八年级', class: '三班', num: '49人' },
          { id: 7, grade: '八年级', class: '一班', num: '58人' }
        ],
        header: {
          studentInfo: [
            { prop: 'id', label: '学号' },
            { prop: 'name', label: '姓名' },
            { prop: 'date', label: '报名日期' },
            { prop: 'height', label: '身高' },
            { prop: 'weight', label: '体重' }
          ],
          classInfo: [
            { prop: 'id', label: '序号' },
            { prop: 'grade', label: '年级' },
            { prop: 'class', label: '班级' },
            { prop: 'num', label: '人数' }
          ]
        },
        multiHeader: {
          studentInfo: [['学生信息'], ['学号', '姓名', '报名日期', '身高', '体重']],
          classInfo: [['班级信息'], ['序号', '年级', '班级', '人数']]
        },
        merges: {
          studentInfo: ['A1:E1'],
          classInfo: ['A1:D1']
        },
        sheetName: {
          studentInfo: '学生信息',
          classInfo: '班级信息'
        }
      }
    },

    methods: {
      exportSuccess(file) {
        console.log('导出成功', file)
      },
      erportError(err) {
        console.log('导出失败', err)
      }
    }
  }
</script>
```

:::

### Attribute

<!-- prettier-ignore -->
| 参数      | 说明          | 类型      | 可选值                           | 默认值  |
|---------- |-------------- |---------- |--------------------------------  |-------- |
| data | 需要导出的数据,如果是Function类型，返回值需要是一个数组或者对象，单工作表时传Array，多工作表时传Object | Array / Function / Object | — | — |
| header | 导出文件的表头,每一项是含有prop(必填)，lable(必填)，wch(可选)的对象，分别表示字段名，表头标题，当前列宽度，单工作表时传Array，多工作表时传Object | Array / Object | — | — |
| fileName | 导出的文件名称 | String | — | Excel |
| sheetName | 工作表的名称 | String / Object | — | 单工作表的时候：默认为'Sheet1',多工作表的时候：默认为data中的key值 |
| multiHeader | 自定义表头,数组的每一项代表一行，单工作表时传Array，多工作表时传Object | Array / Object | — | — |
| merges | 需要合并的单元格,如['A1:A2','B2:C3'],代表A1，A2合并为一个单元格,B2,B3,C2,C3合并为一个单元格，单工作表时传Array，多工作表时传Object | Array / Object | — | — |
| on-import-success | 导入成功回调 | Function(data,header) | — | — |
| on-import-error | 导入失败回调 | Function(error) | — | — |
| on-export-success | 导出成功回调 | Function(file) | — | — |
| on-export-error | 导出失败回调 | Function(error) | — | — |
| download | 导出时是否触发文件下载 | Boolean | — | true |

### Slot

<!-- prettier-ignore -->
| name | 说明 |
|------|--------|
| import | 触发文件导入的内容 |
| export | 触发文件导出的内容 |
| — | 如果没有指定import和export插槽，默认为触发文件导出的内容 |

### Methods

<!-- prettier-ignore -->
| 方法名      | 说明          | 参数 |
|----------- |-------------- | -- |
| handleImport | 文件导入 | file |
| handleExport | 文件导出 | — |
