/*global Vue,VueRouter*/
import Layout from '../layout'
import Wellcome from '../views/wellcome'

Vue.use(VueRouter)

export const routes = [
  {
    path: '/',
    meta: { title: 'element-ui-sup', hidden: true },
    component: Wellcome
  },
  {
    path: '/quickstart',
    component: Layout,
    meta: { title: '快速开始' },
    children: [
      {
        path: '/quickstart',
        name: 'quickstart',
        component: () => import('@/views/quickstart.md')
      }
    ]
  },
  {
    path: '/icon',
    component: Layout,
    meta: { title: '图标库' },
    children: [
      {
        path: '/icon',
        name: 'icon',
        component: () => import('@/views/icon.md')
      }
    ]
  },
  {
    path: '/paginationTable',
    component: Layout,
    meta: { title: '分页表格' },
    children: [
      {
        path: '/paginationTable',
        name: 'paginationTable',
        component: () => import('@/views/paginationTable/paginationTable.md')
      }
    ]
  },
  {
    path: '/upload',
    component: Layout,
    meta: { title: '大文件分片上传' },
    children: [
      {
        path: '/upload',
        name: 'upload',
        component: () => import('@/views/upload.md')
      }
    ]
  },
  {
    path: '/imageCut',
    component: Layout,
    meta: { title: '图片裁切' },
    children: [
      {
        path: '/imageCut',
        name: 'imageCut',
        component: () => import('@/views/imageCut.md')
      }
    ]
  },
  {
    path: '/excel',
    component: Layout,
    meta: { title: 'excel导入导出' },
    children: [
      {
        path: '/excel',
        name: 'excel',
        component: () => import('@/views/excel.md')
      }
    ]
  }
]
//生产环境不加载测试页面的路由
if (process.env.NODE_ENV === 'development') {
  routes.unshift({
    path: '/test',
    component: Layout,
    meta: { title: '开发调试页' },
    children: [
      {
        path: '/test',
        name: 'test',
        component: () => import('@/views/test.vue')
      }
    ]
  })
}
const router = new VueRouter({
  routes
})

export default router
