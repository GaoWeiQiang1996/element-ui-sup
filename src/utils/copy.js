// 点击复制功能
import Clipboard from 'clipboard'
const Message = window.ELEMENT.Message
export default function (event, success, fail) {
  const clipboard = new Clipboard(event.currentTarget)
  clipboard.on('success', (e) => {
    success ? success(e) : Message.success('复制成功')
    // 释放内存
    clipboard.destroy()
  })
  clipboard.on('error', (e) => {
    fail ? fail(e) : Message.error('该浏览器不支持自动复制')
    // 释放内存
    clipboard.destroy()
  })
  clipboard.onClick(event)
}
