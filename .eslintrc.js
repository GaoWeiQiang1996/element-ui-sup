module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    es6: true,
    amd: true
  },
  extends: ['eslint:recommended', 'plugin:vue/recommended', 'plugin:prettier/recommended'],
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    parser: 'babel-eslint',
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    }
  },
  plugins: ['html', 'json', 'vue', 'prettier', 'markdown'],
  rules: {
    //允许一行有多个属性
    'vue/max-attributes-per-line': [
      'error',
      {
        singleline: 99,
        multiline: {
          max: 99,
          allowFirstLine: false
        }
      }
    ],
    //强制空标签自动闭合
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    //关闭所有标签必须换行
    'vue/singleline-html-element-content-newline': 'off',
    'vue/require-default-prop': 'off',
    'vue/require-prop-types': 'error',
    'vue/no-v-html': 'off'
  }
}
