## Upload 大文件分片上传

将大文件切割成多个小文件进行上传

### 基本使用

:::demo

```html
<els-upload class="upload-demo" action="http://192.168.28.103:10001/file/upload" :on-preview="handlePreview" :on-remove="handleRemove" :before-remove="beforeRemove" multiple :limit="3" :on-exceed="handleExceed" :on-success="handleSuccess" :on-error="handleError" :data="chunkData">
  <el-button size="small" type="primary">点击上传</el-button>
  <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且不超过500kb</div>
</els-upload>
<script>
  export default {
    methods: {
      chunkData(option) {
        return {
          size: option.fileSize, // 总文件大小
          chunks: option.chunkTotal, // 所有切片数量
          chunk: option.chunkIndex, // 当前切片下标
          md5: option.chunkHash, // 单个切片hash
          filename: option.fileName, // 文件名
          fileHash: option.fileHash // 整个文件hash
        }
      },
      handleSuccess(response, file, fileList) {
        //文件上传成功
        console.log(response, file, fileList)
      },
      handleError(err, file, fileList) {
        //文件上传失败
        console.log(err, file, fileList)
      },
      handleRemove(file, fileList) {
        //移除文件
        console.log(file, fileList)
      },
      handlePreview(file) {
        //点击文件
        console.log(file)
      },
      handleExceed(files, fileList) {
        this.$message.warning(`当前限制选择 3 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length + fileList.length} 个文件`)
      },
      beforeRemove(file, fileList) {
        //移除文件之前的钩子
        return this.$confirm(`确定移除 ${file.name}？`)
      }
    }
  }
</script>
```

:::

### 手动上传

:::demo

```html
<els-upload class="upload-demo" ref="upload" action="http://192.168.28.103:10001/file/upload" :on-preview="handlePreview" :on-remove="handleRemove" :auto-upload="false" :data="chunkData">
  <el-button slot="trigger" size="small" type="primary">选取文件</el-button>
  <el-button style="margin-left: 10px;" size="small" type="success" @click="submitUpload">上传到服务器</el-button>
  <div slot="tip" class="el-upload__tip">只能上传jpg/png文件，且不超过500kb</div>
</els-upload>
<script>
  export default {
    methods: {
      chunkData(option) {
        return {
          size: option.fileSize, // 总文件大小
          chunks: option.chunkTotal, // 所有切片数量
          chunk: option.chunkIndex, // 当前切片下标
          md5: option.chunkHash, // 单个切片hash
          filename: option.fileName, // 文件名
          fileHash: option.fileHash // 整个文件hash
        }
      },
      submitUpload() {
        this.$refs.upload.submit()
      },
      handleRemove(file, fileList) {
        console.log(file, fileList)
      },
      handlePreview(file) {
        console.log(file)
      }
    }
  }
</script>
```

:::

### Attribute

<!-- prettier-ignore -->
| 参数      | 说明          | 类型      | 可选值                           | 默认值  |
|---------- |-------------- |---------- |--------------------------------  |-------- |
| action | 必选参数，上传的地址 | string | — | — |
| headers | 设置上传的请求头部 | object | — | — |
| multiple | 是否支持多选文件 | boolean | — | — |
| data | 返回类型是一个对象，这个对象内容就是每个分片携带的额外参数，参数option是一个对象，里面有chunkSize（当前分片的大小），chunkTotal（分片总数），chunkIndex（分片下标），chunkHash（分片hash），fileName（文件名），fileHash（文件hash），fileSize（文件大小）七个属性 | function(option) | — | — |
| chunk-size| 每块切片的大小，单位 B，如 5MB 需要写成 1024\*1024\*5| number | — | 1024\*1024\*10 |
| thread| 线程，允许同一时间上传的分片数量| number | — | 3 |
| name | 上传的文件字段名 | string | — | file |
| with-credentials | 支持发送 cookie 凭证信息 | boolean | — | false |
| show-file-list | 是否显示已上传文件列表 | boolean | — | true |
| drag | 是否启用拖拽上传 | boolean | — | false |
| accept | 接受上传的[文件类型](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#attr-accept) | string | — | — |
| on-preview | 点击文件列表中已上传的文件时的钩子 | function(file) | — | — |
| on-remove | 文件列表移除文件时的钩子 | function(file, fileList) | — | — |
| on-success | 文件上传成功时的钩子 | function(response, file, fileList) | — | — |
| on-error | 文件上传失败时的钩子 | function(err, file, fileList) | — | — |
| on-progress | 文件上传时的钩子 | function(event, file, fileList) | — | — |
| on-change | 文件状态改变时的钩子，添加文件、上传成功和上传失败时都会被调用 | function(file, fileList) | — | — |
| before-upload | 上传文件之前的钩子，参数为上传的文件，若返回 false 或者返回 Promise 且被 reject，则停止上传。 | function(file) | — | — |
| before-remove | 删除文件之前的钩子，参数为上传的文件和文件列表，若返回 false 或者返回 Promise 且被 reject，则停止删除。| function(file, fileList) | — | — |
| list-type | 文件列表的类型 | string | text/picture/picture-card | text |
| auto-upload | 是否在选取文件后立即进行上传 | boolean | — | true |
| file-list | 上传的文件列表, 例如: [{name: 'food.jpg', url: 'https://xxx.cdn.com/xxx.jpg'}] | array | — | [] |
| http-request | 覆盖默认的上传行为，可以自定义上传的实现 | function | — | — |
| disabled | 是否禁用 | boolean | — | false |
| limit | 最大允许上传个数 |  number | — | — |
| on-exceed | 文件超出个数限制时的钩子 | function(files, fileList) | — | - |

### Slot

<!-- prettier-ignore -->
| name | 说明 |
|------|--------|
| trigger | 触发文件选择框的内容 |
| tip | 提示说明文字 |

### Methods

<!-- prettier-ignore -->
| 方法名      | 说明          | 参数 |
|----------- |-------------- | -- |
| clearFiles | 清空已上传的文件列表（该方法不支持在 before-upload 中调用） | — |
| abort      | 取消上传请求    | （ file: fileList 中的 file 对象 ） |
| submit     | 手动上传文件列表 |  —                                |
