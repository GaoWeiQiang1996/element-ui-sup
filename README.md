# element-ui-sup

### vscode 推荐配置

```json
{
  "editor.formatOnSave": true,
  "eslint.validate": ["javascript", "javascriptreact", "vue", "html", "markdown"],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "[jsonc]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[markdown]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[vue]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[html]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  }
}
```

### 安装依赖

```
npm install
```

### 启动项目

```
npm run dev
```

### 构建项目

```
npm run build
```

### 发布项目

```
npm publish
```

### 按 eslint 规则格式化所有代码

```
npm run lint
```
