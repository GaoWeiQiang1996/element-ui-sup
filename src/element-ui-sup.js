import utils from './utils/index'
import './styles/flex.scss'
const modulesFiles = require.context('./packages', true, /index.js/)
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // 将./input/index.js 转成 input
  const moduleName = modulePath.split('/')[1]
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})
const components = Object.values(modules)
// 定义 install 方法，接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const install = (Vue) => {
  // 判断是否可以安装
  if (install.installed) return
  // 遍历注册全局组件
  components.forEach((component) => Vue.component(component.name, component))
  // 注册公用方法
  Vue.prototype.$utils = utils
}
// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

modules.install = install

export default modules
