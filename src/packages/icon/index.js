// 导入的组件必须在vue文件中声明 name, 否则组件无法注册使用
import icon from './icon'
const req = require.context('../../icon', false, /\.svg$/)
const requireAll = (requireContext) => requireContext.keys().map(requireContext)
requireAll(req)
// 为组件提供 install 安装方法，供按需引入
icon.install = (Vue) => {
  Vue.component(icon.name, icon)
}

// 默认导出组件
export default icon
