import formatTime from './formatTime'
import copy from './copy'

const util = {
  formatTime,
  copy
}

export default util
