/*global Vue,hljs*/
import App from './App'
import router from './router'
import ElementUiSup from 'element-ui-sup'
import 'normalize.css/normalize.css'
import './styles/index.scss'
import demoBlock from './components/demo-block.vue'

Vue.component('DemoBlock', demoBlock)
Vue.use(ElementUiSup)

Vue.config.productionTip = false
router.afterEach(() => {
  Vue.nextTick(() => {
    const blocks = document.querySelectorAll('pre code:not(.hljs)')
    Array.prototype.forEach.call(blocks, hljs.highlightBlock)
  })
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
