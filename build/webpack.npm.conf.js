'use strict'
const utils = require('./utils')
const config = require('../config')
const merge = require('webpack-merge')
const baseWebpackConfig = require('./webpack.base.conf')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const entry = utils.getComponentEntries('src/packages')
entry.index = utils.resolve('src/element-ui-sup.js')
const webpackConfig = merge(baseWebpackConfig, {
  mode: 'production',
  entry: entry,
  output: {
    path: config.npm.assetsRoot,
    filename: '[name].js',
    chunkFilename: 'asyncChunk/[name].js',
    publicPath: config.npm.assetsPublicPath,
    libraryTarget: 'umd',
    libraryExport: 'default',
    library: 'element-ui-sup',
    umdNamedDefine: true
  },

  module: {
    rules: utils.styleLoaders({
      sourceMap: config.npm.productionSourceMap,
      extract: true,
      usePostCSS: true
    })
  },
  devtool: config.npm.productionSourceMap ? config.npm.devtool : false,

  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: config.npm.productionSourceMap,
        uglifyOptions: {
          warnings: false
        }
      }),
      new OptimizeCSSPlugin({
        cssProcessorOptions: config.npm.productionSourceMap ? { safe: true, map: { inline: false } } : { safe: true }
      })
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style/[name].css',
      chunkFilename: 'style/asyncStyle/[contenthash].css' // use contenthash *
    })
  ]
})

if (config.npm.productionGzip) {
  const CompressionWebpackPlugin = require('compression-webpack-plugin')
  webpackConfig.plugins.push(
    new CompressionWebpackPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: new RegExp('\\.(' + config.npm.productionGzipExtensions.join('|') + ')$'),
      threshold: 10240,
      minRatio: 0.8
    })
  )
}

if (config.npm.bundleAnalyzerReport) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
  webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}

module.exports = webpackConfig
