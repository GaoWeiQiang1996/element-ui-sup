// 导入的组件必须在vue文件中声明 name, 否则组件无法注册使用
import excel from './excel'

// 为组件提供 install 安装方法，供按需引入
excel.install = (Vue) => {
  Vue.component(excel.name, excel)
}

// 默认导出组件
export default excel
