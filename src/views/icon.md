## Icon 图标

提供了一套常用的图标集合。

### 使用方法

:::tip
通过 `<els-icon :icon="图标名字" size="30px" color="#666" />` 来使用，传入`size`来控制图标大小，传入`color`来控制图标颜色
:::

:::demo

```html
<els-icon icon="android" size="20px" color="#666666" />
<els-icon icon="chrome-fill" size="30px" color="red" />
<els-icon icon="comment" size="50px" color="#3d7eff" />
```

:::

### 图标集合

:::tip
点击下面图标可复制
:::

:::demo

```html
<div class="icon-container">
  <div class="flex flex-start flex-wrap">
    <div v-for="icons in icons" :key="icons" class="icon-item" title="点击复制" :data-clipboard-text="icons" @click="copy($event)">
      <els-icon :icon="icons" size="36px" class="icon"></els-icon>
      <div class="icon-name">{{ icons }}</div>
    </div>
  </div>
</div>
<script>
  //获取src/icon目录下的所有svg文件
  const modulesFiles = require.context('../icon', true, /\.svg$/)
  const modules = modulesFiles.keys().map((modules) => {
    return modules.replace(/.*\/([^/]+).svg*/g, '$1')
  })

  export default {
    computed: {
      icons() {
        return modules
      }
    },
    methods: {
      copy(event) {
        this.$utils.copy(event)
      }
    }
  }
</script>
```

:::
