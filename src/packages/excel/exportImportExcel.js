import XLSX from 'xlsx'
import { saveAs } from 'file-saver'

//二进制转为ArrayBuffer
function s2ab(s) {
  const buf = new ArrayBuffer(s.length)
  const view = new Uint8Array(buf)
  for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xff
  return buf
}
//获取excel表头
function getHeaderRow(sheet) {
  const headers = []
  const range = XLSX.utils.decode_range(sheet['!ref'])
  let C
  const R = range.s.r
  const headersRepeatNum = {}
  /* start in the first row */
  for (C = range.s.c; C <= range.e.c; ++C) {
    /* walk every column in the range */
    const cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })]
    /* find the cell in the first row */
    let hdr = 'UNKNOWN ' + C // <-- replace with your desired default
    if (cell && cell.t) {
      hdr = XLSX.utils.format_cell(cell)
      //如果有重复的，就在后面加上  _{下标}
      if (hdr in headersRepeatNum) {
        hdr = hdr + '_' + headersRepeatNum[hdr]++
      } else {
        headersRepeatNum[hdr] = 1
      }
    }
    headers.push(hdr)
  }
  return headers
}
//excel中的时间转换为Date 如 44480.865208333336 => Mon Oct 11 2021 20:45:54 GMT+0800 (中国标准时间)
function numToDate(num) {
  const date = new Date('1970/1/1')
  const time = Math.round((num % 1) * 86400)
  date.setDate(Math.floor(num) - 25568)
  date.setSeconds(time)
  return date
}
//Date对象转为excel表格中的时间 如 Mon Oct 11 2021 20:45:54 GMT+0800 (中国标准时间) => 44480.865208333336
function dateToNum(v) {
  const basedate = new Date('1970/1/1').getTime()
  const epoch = v.getTime()
  return (epoch - basedate) / (24 * 60 * 60 * 1000) + 25569
}
//改写XLSX库中的XLSX.utils.aoa_to_sheet方法，修复时间多出43秒的BUG
function aoa_to_sheet(data) {
  const ws = {}
  const range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } }
  for (let R = 0; R != data.length; ++R) {
    for (let C = 0; C != data[R].length; ++C) {
      const cell = { v: data[R][C] }
      if (range.s.r > R) range.s.r = R
      if (range.s.c > C) range.s.c = C
      if (range.e.r < R) range.e.r = R
      if (range.e.c < C) range.e.c = C
      if (cell.v === null) {
        continue
      }
      if (typeof cell.v === 'number') cell.t = 'n'
      else if (typeof cell.v === 'boolean') cell.t = 'b'
      else if (cell.v instanceof Date) {
        cell.z = XLSX.SSF._table[22]
        cell.t = 'n'
        cell.v = dateToNum(cell.v)
        cell.w = XLSX.SSF.format(cell.z, cell.v)
      } else {
        cell.t = 's'
      }
      const cell_ref = XLSX.utils.encode_cell({ c: C, r: R })
      ws[cell_ref] = cell
    }
  }
  if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range)
  return ws
}
//导出excel表格
export function exportExcel({ data, fileName = 'Excel', merges, cols, download } = {}) {
  const wb = XLSX.utils.book_new()
  Object.keys(data).forEach((key) => {
    // const ws = XLSX.utils.aoa_to_sheet(data[key])
    const ws = aoa_to_sheet(data[key])
    if (merges[key].length > 0) {
      if (!ws['!merges']) {
        ws['!merges'] = []
      }
      merges[key].forEach((item) => {
        ws['!merges'].push(XLSX.utils.decode_range(item))
      })
    }
    if (cols[key]) {
      ws['!cols'] = cols[key]
    }
    wb.SheetNames.push(key)
    wb.Sheets[key] = ws
  })

  const blob = s2ab(XLSX.write(wb, { bookType: 'xlsx', bookSST: false, type: 'binary' }))
  const file = new File([blob], fileName + '.xlsx', { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
  if (download) {
    saveAs(file)
  }
  return file
}
//导入excel表格
export function importExcel({ file, header } = {}) {
  return new Promise((resolve) => {
    const reader = new FileReader()
    reader.onload = (e) => {
      const data = e.target.result
      const workbook = XLSX.read(data, { type: 'array' })
      //读取第一张工作表
      const firstSheetName = workbook.SheetNames[0]
      const worksheet = workbook.Sheets[firstSheetName]
      //处理excel中的时间格式
      Object.keys(worksheet).forEach((key) => {
        if (key.indexOf('!') !== -1) return
        let { t, w, v } = worksheet[key]
        if (t === 'n' && v.toString() !== w) {
          worksheet[key].t = 'd'
          worksheet[key].v = numToDate(v)
        }
      })

      //处理表格中合并的单元格
      if (worksheet['!merges'] && worksheet['!merges'].length > 0) {
        const merges = worksheet['!merges']
        merges.forEach((item) => {
          const cell = XLSX.utils.encode_cell(item.s)
          for (let R = item.s.r; R <= item.e.r; R++) {
            for (let C = item.s.c; C <= item.e.c; C++) {
              const currentCell = XLSX.utils.encode_cell({ r: R, c: C })
              if (currentCell !== cell) {
                worksheet[currentCell] = worksheet[cell]
              }
            }
          }
        })
      }
      let results = XLSX.utils.sheet_to_json(worksheet)
      //建立header中label与prop的映射
      const headerMap = {}
      header.forEach((item) => {
        headerMap[item.label] = item.prop
      })
      //如果表头的值与lable相同，就将字段替换成header的prop,不存在就使用原先的
      results = results.map((item) => {
        const resultItem = {}
        Object.keys(item).forEach((itemKey) => {
          if (itemKey in headerMap) {
            resultItem[headerMap[itemKey]] = item[itemKey]
          } else {
            resultItem[itemKey] = item[itemKey]
          }
        })
        return resultItem
      })
      //获取表格表头，加工成table能使用的数据
      let headerResults = getHeaderRow(worksheet)
      headerResults = headerResults.map((item) => {
        if (item in headerMap) {
          return {
            prop: headerMap[item],
            label: item
          }
        } else {
          return { prop: item, label: item }
        }
      })
      resolve({ header: headerResults, data: results })
    }
    reader.readAsArrayBuffer(file)
  })
}
